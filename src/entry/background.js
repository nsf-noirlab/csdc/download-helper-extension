let _files = [];
let _maxConcurrent = 5;
let _status = {};
var client;

if (navigator.userAgent.includes('Firefox')) {
  client = browser;
}else{
  client = chrome;
}

function setBadge(value) {
  if (client.action) {
    // v3
    client.action.setBadgeText(value);
  } else {
    // v2 (FF etc)
    client.browserAction.setBadgeText(value);
  }
}

function download(fileObj) {
  if(!fileObj){ return 0; }
  if(_files.length === 0){ return 0; }
  client.downloads.download({
    url: fileObj.retrieve_url,
    filename: `csdc_downloads/${fileObj.filename}`,
    conflictAction: 'overwrite'
  }, (downloadid) => {
    _status[downloadid] = {...fileObj};
  });
}

function cancelDownloads() {
  client.downloads.search({}, (Items) => {
    console.log(Items);
    for( let item of Items ){
      if(item.state === 'in_progress'){
        client.downloads.cancel(item.id);
      }
    }
  });
}
function updateStatus() {
  client.downloads.search({}, (DlItems) => {
    for(let i in DlItems){
      if( i.error ) {
        console.error(i.error);
        if( i.canResume ){
          client.downloads.resume(i.id);
        }else{
          _status[i.id].error = i.error;
          _status[i.id].failed = true;
          client.storage.local.set({failed: 'Some files failed to download'});
          return 0;
        }
      }
      if( _status[i.id]  ){
        _status[i.id].bytesReceived = i.bytesReceived;
      }
    }
  });
}

function clearManifest() {
  _files = [];
  client.storage.local.set({files: JSON.stringify(_files)});
  client.storage.local.set({fileCount: {totalFiles: 0, completedCount: 0}});
  setBadge({tabId: null, text: ''});
}

client.runtime.onInstalled.addListener(() => {
});

client.downloads.onChanged.addListener( (delta) => {

  if( delta.state && (delta.state.current === 'complete') ){
    //!console.log(_status);
    //!console.log(delta);
    _status[delta.id].complete = true;
    _status[delta.id].endTime = delta.endTime;

    if( _files.length > 0 ){
      let nextFile = _files.pop();
      client.storage.local.set({'files': JSON.stringify(_files)});
      download(nextFile);

      setBadge({tabId: null, text: _files.length+''});
      client.runtime.sendMessage({'action': 'fileDone', 'filename': _status[delta.id].filename});

    }

    let in_prog = false;
    for( let f in _status ){
      if( _status[f].state === 'in_progress' ){
        in_prog = true;
        break;
      }
    }

    if( !in_prog ){// no more in progress
      client.runtime.sendMessage({'action': 'allDone'});
      client.storage.local.set({'canPause': false, 'canResume': false});
    }
  }
});

client.downloads.onCreated.addListener( (downloadItem) => {
  if(downloadItem.id){
    if(!_status[downloadItem.id]){
      _status[downloadItem.id] = {};
    }
    _status[downloadItem.id].size = downloadItem.fileSize;
  }
});

client.runtime.onMessage.addListener((request) => {  //, sender, sendResponse) => {
  // communication tree
  // start download
  if (request.action && request.action === 'startDownload') {
    client.storage.local.set({'canPause': true, 'canResume': false});
    seedDownload();
  }
  // pause
  if (request.action && request.action === 'pause') {
    client.storage.local.set({'canPause': false, 'canResume': true});
    client.downloads.search({}, (DlItems) => {
      for( let item of DlItems ){
        if(item.state === 'in_progress'){
          client.downloads.pause(item.id);
        }
      }
    });
  }
  if (request.action && request.action === 'resume') {
    client.storage.local.set({'canPause': true, 'canResume': false});
    client.downloads.search({}, (DlItems) => {
      for (let item of DlItems) {
        if (item.paused === true) {
          client.downloads.resume(item.id);
        }
      }
    });
  }
  // manifest file loaded
  if (request.action && request.action === 'manifestFileLoaded') {
    _files = request.files;
    const totalFiles = _files.length;
    client.storage.local.set({files: JSON.stringify(_files)});
    client.storage.local.set({'fileCount': {'totalFiles': totalFiles, 'completedCount': 0}});
    setBadge({tabId: null, text: _files.length+''});
  }
  // download manifest from staging/downloads page
  if (request.action && request.action === 'downloadFromStaging') {
    client.storage.local.set({'canPause': true, 'canResume': false});
    _files = request.data;
    //!console.log(request.data);
    client.storage.local.set({'files': JSON.stringify(_files)});
    client.storage.local.set({'fileCount': {'totalFiles': _files.length, 'completedCount': 0}});
    client.runtime.sendMessage({'action':'manifestLoaded'});
    seedDownload();
  }

  if(request.action && request.action === 'cancel') {
    cancelDownloads();
  }
  // clear manifest
  if (request.action && request.action === 'clearManifest') {
    client.storage.local.set({'canPause':false, 'canResume': false});
    cancelDownloads();
    clearManifest();
  }
  //return true;
});

function seedDownload() {
  for(let idx = 0; idx< _maxConcurrent && _files.length > 0; idx++){
      download(_files.pop());
    }
    setTimeout(updateStatus, 300);
}
