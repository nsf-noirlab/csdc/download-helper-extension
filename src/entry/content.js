
let downloadButtonInjected = false;
let poll = setInterval(function(){ injectDownloadButton(); }, 1000);
const browser = (chrome) ? chrome : browser;

/*
  This chunk of code is just to track when the page changes, since there isn't an event trigger
  for url changes....
 */
var oldLength = -1;
function listen(currentLength) {
  if (currentLength != oldLength) {
    // This is to reset things since the buttom might unload we want to inject it again
    // if we go back to the downloads page
    oldLength = currentLength;
    downloadButtonInjected = false;
    clearInterval(poll);
    poll = setInterval(function(){ injectDownloadButton(); }, 1000);
  }

  oldLength = window.history.length;
  setTimeout(function () {
    listen(window.history.length);
  }, 500);
}
listen(window.history.length);

// Injects the download button
// checks that we haven't already injected it or
// if the page content is ready (i.e. still loading)
function injectDownloadButton() {
  if( downloadButtonInjected ){
    return false;
  }
  // may not have files ready for download so drop out
  const element = document.querySelector(".staged-file-list");
  if( !element ){
    return false;
  }

  // page is ready
  downloadButtonInjected = true;
  clearInterval(poll);

  const btnExists = document.querySelector(".extension-button");
  if ( btnExists ) {
    return false;
  }

  let div = document.createElement("div");
  let button = document.createElement("button");
  let logo = browser.runtime.getURL('/icons/icon-16x16.png');
  let img = document.createElement('img');
  img.setAttribute('src', logo);
  img.style.verticalAlign = "middle";
  button.className = "btn btn-success";

  button.appendChild(img);
  let textSpan = document.createElement('span');
  textSpan.style.verticalAlign = "middle";
  textSpan.innerText = ' Download with helper extension';
  button.appendChild(textSpan);

  button.onclick = (event) => {
    event.preventDefault();
    fetch(`${window.location.origin}/portal/download_manifest/json`).then((response) => {
      if(!response.ok){
        console.error("Invalid response from get_staged_list");
        return response.blob();
      }else{
        return response.json();
      }
    }).then((data) => {
      // call manifestLoaded to inform the popop
      browser.runtime.sendMessage({'action': 'downloadFromStaging', 'data': data});
    });
  }

  div.className = "text-center mb-2 extension-button";
  div.appendChild(button);

  let alertPanel = document.querySelector(".extension-helper");
  if (alertPanel){
    alertPanel.replaceWith(div);
  }else{
    document.querySelector(".tab-portal-pane .alert.alert-info").after(div);
  }
}

