const path = require('path')
const fs = require('fs')

// Generate pages object
const pages = {}

function getEntryFile (entryPath) {
  let files = fs.readdirSync(entryPath)
  return files
}

const chromeName = getEntryFile(path.resolve(`src/entry`))

function getFileExtension (filename) {
  return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined
}
chromeName.forEach((name) => {
  const fileExtension = getFileExtension(name)
  const fileName = name.replace('.' + fileExtension, '')
  pages[fileName] = {
    entry: `src/entry/${name}`,
    template: 'public/index.html',
    filename: `${fileName}.html`
  }
})

const isDevMode = process.env.NODE_ENV === 'development'
const moz = process.env.PLATFORM === 'mozilla'

module.exports = {
  pages,
  outputDir: (moz) ? 'mozilla_dist' : 'dist',
  filenameHashing: false,
  chainWebpack: (config) => {
    config.plugin('copy').use(require('copy-webpack-plugin'), [
      {
        patterns: [
          {
            from: path.resolve(`src/manifest.${process.env.NODE_ENV}.json`),
            to: `${path.resolve('dist')}/manifest.json`
          },
          {
            from: path.resolve(`public/`),
            to: `${path.resolve('dist')}/`
          },
          {
            from: path.resolve('src/mozilla.manifest.json'),
            to: `${path.resolve('mozilla_dist')}/manifest.json`
          },
          {
            from: path.resolve('public/'),
            to: `${path.resolve('mozilla_dist')}/`
          },
          /*
          {
            from: path.resolve(`build/`),
            to: `${path.resolve('dist')}/`
          },
          {
            from: path.resolve(`build/`),
            to: `${path.resolve('mozilla_dist')}/`
          }
          */
        ]
      }
    ])
  },
  configureWebpack: {
    output: {
      filename: `[name].js`,
      chunkFilename: `[name].js`
    },
    devtool: isDevMode ? 'inline-source-map' : false
  },
  css: {
    extract: false // Make sure the css is the same
  }
}
