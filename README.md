# NOIRLab CSDC Download Helper Extension 

The goal of this extension is to aid in downloading potentially a large number of files from the archive. 
This is meant to be a better, more streamlined experience than FTP (which is being fazed out).

This is made as a cross-browser extension, with minimal difference between versions. Currently we are only
supporting Chrome based browsers and Mozilla based browsers. 

### Libraries/Tools used 
* Vuejs
* Webpack
* Yarn

## Project setup
``` sh
yarn install
```

### Compiles and hot-reloads for development
``` sh
yarn serve
```

### Compiles and minifies for production
``` sh
yarn build # chrome 
```

``` sh
yarn build-moz # firefox
```

### Develop 

``` sh
yarn build-watch # chrome 
```

``` sh
yarn build-watch-moz # firefox
```


### Lints and fixes files
``` sh
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
