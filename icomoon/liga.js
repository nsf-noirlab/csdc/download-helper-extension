/* A polyfill for browsers that don't support ligatures. */
/* The script tag referring to this file must be placed before the ending body tag. */

/* To provide support for elements dynamically added, this script adds
   method 'icomoonLiga' to the window object. You can pass element references to this method.
*/
(function () {
    'use strict';
    function supportsProperty(p) {
        var prefixes = ['Webkit', 'Moz', 'O', 'ms'],
            i,
            div = document.createElement('div'),
            ret = p in div.style;
        if (!ret) {
            p = p.charAt(0).toUpperCase() + p.substr(1);
            for (i = 0; i < prefixes.length; i += 1) {
                ret = prefixes[i] + p in div.style;
                if (ret) {
                    break;
                }
            }
        }
        return ret;
    }
    var icons;
    if (!supportsProperty('fontFeatureSettings')) {
        icons = {
            'home': '&#xe900;',
            'house': '&#xe900;',
            'pencil': '&#xe905;',
            'write': '&#xe905;',
            'pacman': '&#xe916;',
            'game2': '&#xe916;',
            'books': '&#xe920;',
            'library': '&#xe920;',
            'files-empty': '&#xe925;',
            'files': '&#xe925;',
            'folder': '&#xe92f;',
            'directory': '&#xe92f;',
            'folder-open': '&#xe930;',
            'directory2': '&#xe930;',
            'folder-plus': '&#xe931;',
            'directory3': '&#xe931;',
            'folder-minus': '&#xe932;',
            'directory4': '&#xe932;',
            'folder-download': '&#xe933;',
            'directory5': '&#xe933;',
            'folder-upload': '&#xe934;',
            'directory6': '&#xe934;',
            'clock': '&#xe94e;',
            'time2': '&#xe94e;',
            'download': '&#xe960;',
            'save': '&#xe960;',
            'upload': '&#xe961;',
            'load': '&#xe961;',
            'database': '&#xe964;',
            'db': '&#xe964;',
            'undo': '&#xe965;',
            'ccw': '&#xe965;',
            'redo': '&#xe966;',
            'cw': '&#xe966;',
            'bubble': '&#xe96b;',
            'comment': '&#xe96b;',
            'spinner2': '&#xe97b;',
            'loading3': '&#xe97b;',
            'search': '&#xe986;',
            'magnifier': '&#xe986;',
            'cog': '&#xe994;',
            'gear': '&#xe994;',
            'rocket': '&#xe9a5;',
            'jet': '&#xe9a5;',
            'meter': '&#xe9a6;',
            'gauge': '&#xe9a6;',
            'fire': '&#xe9a9;',
            'flame': '&#xe9a9;',
            'bin2': '&#xe9ad;',
            'trashcan2': '&#xe9ad;',
            'warning': '&#xea07;',
            'sign': '&#xea07;',
            'cancel-circle': '&#xea0d;',
            'close': '&#xea0d;',
            'blocked': '&#xea0e;',
            'forbidden': '&#xea0e;',
            'checkmark': '&#xea10;',
            'tick': '&#xea10;',
            'play2': '&#xea15;',
            'player': '&#xea15;',
            'pause': '&#xea16;',
            'player2': '&#xea16;',
            'stop': '&#xea17;',
            'player3': '&#xea17;',
          '0': 0
        };
        delete icons['0'];
        window.icomoonLiga = function (els) {
            var classes,
                el,
                i,
                innerHTML,
                key;
            els = els || document.getElementsByTagName('*');
            if (!els.length) {
                els = [els];
            }
            for (i = 0; ; i += 1) {
                el = els[i];
                if (!el) {
                    break;
                }
                classes = el.className;
                if (/icon/.test(classes)) {
                    innerHTML = el.innerHTML;
                    if (innerHTML && innerHTML.length > 1) {
                        for (key in icons) {
                            if (icons.hasOwnProperty(key)) {
                                innerHTML = innerHTML.replace(new RegExp(key, 'g'), icons[key]);
                            }
                        }
                        el.innerHTML = innerHTML;
                    }
                }
            }
        };
        window.icomoonLiga();
    }
}());
